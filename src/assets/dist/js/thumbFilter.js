angular.module('thumb', []).filter('thumb', function () {
  return function (input, params) {
    // thumbnailer location
    var thumbnailer = 'https://thm.arosoftware.com';
    // fetch thumbnail params
    params = params ? '.' + params : '';
    // strip the http
    input = input.replace(/^https{0,1}:\/\//gi, '');
    // strip out the tn.aroso... url if present
    input = input.replace(/(\/\/tn\.arosoftware\.com\/?)/, '');
    // url encode spaces
    input = input.replace(/%20/g, '+');
    input = input.replace(/ /g, '+');
    // cut the extension off the end
    input = input.split(/\./);
    var extension = input.pop();
    input = input.join('.');
    // generate new thumbnailed image url. url will look something like this:
    // http://tn.arosoftware.com/www.arosoftware.com/aro364/upload/images/168001-169000/img168250.w.320.h.240.jpg
    input = thumbnailer + '/' + input + params + '.' + extension;
    return input;
  };
});
