// document.getElementById("CopyrightYear").innerHTML = new Date().getFullYear();
setInterval(function(){ 
    $(function() {
        var owl = $('#featuredPropr');
        if (owl.length==0) {return}
        owl.owlCarousel({
            navigation: true,
            pagination: false,
            autoHeight: false,
            autoPlay: false,
            slideSpeed: 1000,
            items: 3,
            itemsScaleUp: false,
            singleItem: false,
            addClassActive: true,
            responsiveRefreshRate: 200,
            theme: 'propertyList-slideshow-grid-details-overlay-7',
            beforeInit: false,
            afterInit: false,
            beforeMove: false,
            afterMove: false,
            startDragging: false,
            afterAction: false,
            afterLazyLoad: false,
            navigationText:["<div class='btn-nav prev-slide'><i class='icon-general-chevron-thin-left'></i></div>","<div class='btn-nav next-slide'><i class='icon-general-chevron-thin-right'></i></div>"],
            itemsCustom: [
                [0, 1],
                [479, 1],
                [668, 1],
                [769, 2],
                [1000, 3],
                [1200, 3],
                [1600, 3]
            ],
            afterAction: function(el) {
                this
                    .$owlItems
                    .removeClass('active');

                this
                    .$owlItems
                    .eq(this.currentItem + 1)
                    .addClass('active');
            }

        });

        // var d = document;
        // var slider = d.getElementById('featuredPropr_navtop');
        // var btnNextEl = slider.querySelector('[data-owlnext]');
        // var btnPrevEl = slider.querySelector('[data-owlprev]');

        // btnPrevEl.addEventListener('click', function() {
        //     owl.trigger('owl.prev');
        // });
        // btnNextEl.addEventListener('click', function() {
        //     owl.trigger('owl.next');
        // });
    });

    $(function() {
        $('.mobile-menu-nav .dropdown-toggle').on('click', function(e) {
            // e.stopImmediatePropagation();
        });
    });

    $('.mobile-menu-nav').mobileMenu();

    

    $('.menu-btn').offCanvas({
      menuDir: "right",
      menuDirClass: "pushy-right"
    });


    $('.menu-btn').click(function(){
        $('nav.pushy').addClass('pushy-open');
        $('.body-wrap').addClass('container-push');
    });
    $('.mobile-menu-close').click(function(){
        $('nav.pushy').removeClass('pushy-open');
        $('.body-wrap').removeClass('container-push');
        $('body').removeClass('pushy-active')
    })


    $(document).ready(function() {
        var $header = $('.header-wrap');

        AroUtility.init('checkHeightChange', $header);

        //
        var checkHeader = setInterval(function() {
            if ($header.hasClass('affix')) {
                $header.css({
                    position: 'fixed'
                });
                clearInterval(checkHeader);
            }
        }, 1000);
    });

    $(function() {
        $('.quick-search-icon').on('click', function() {
            $('#quickSearchFormWrap').aroSearchOverlay();
        });
    });

    $(function() {
        
        if ($('#MobileMenu').length==0) {return}
        $('#MobileMenu').superfish({
            delay: 500 // .5 second delay on mouseout
                ,
            speed: 'fast' // set the sub menu opening speed
                ,
            disableHI: true // disable hover intent (sub menus open faster)
                ,
            cssArrows: false // hide the drop down arrows
                // FIX SUBMENU OFF SCREEN
                ,
            onBeforeShow: function() {
                var $submenu = $(this); // the menu to open. basically the submenu.
                var screenWidth = $(window).width();

                // just making sure that the display is block to get the accurate position. (cause the default was none)
                $submenu.css({
                    display: 'block'
                });

                if($submenu.offset()){
                    var menuOffsetLeft = $submenu.offset().left;
                }
                var menuWidth = $submenu.width();
                var menuOffsetRight = menuOffsetLeft + menuWidth;

                // determine if the submenu is off the screen
                if (menuOffsetRight > screenWidth) {
                    $submenu.addClass('submenu-left');
                }
            }
        });
    });

    // fix a know affix bug where the class gets added on click
    $(function() {
        if ($('.header-wrap').length==0) {return}
        $('.header-wrap').on('affix.bs.affix', function() {
            if (!$(window).scrollTop()) return false;
        });
    });

    $(function() {

        var owl = $('#homeOwlCarosel');

        owl.owlCarousel({
            navigation: false,
            pagination: false,
            autoHeight: false,
            autoPlay: 7000,
            slideSpeed: 1000,
            items: 1,
            itemsScaleUp: true,
            singleItem: true,
            addClassActive: true,
            responsiveRefreshRate: 200,
            theme: 'slideshow-4',
            transitionStyle: 'fade',
            beforeInit: false,
            afterInit: false,
            beforeMove: false,
            afterMove: false,
            startDragging: false,
            afterAction: false,
            afterLazyLoad: false

        });

    });

    // $(function() {
    //     var owl = $('.testmonialSlider');
    //     if (owl.length==0) {return}

    //     owl.owlCarousel({
    //         navigation: false,
    //         pagination: false,
    //         autoHeight: false,
    //         autoPlay: 6000,
    //         slideSpeed: 2000,
    //         items: 1,
    //         itemsScaleUp: true,
    //         singleItem: true,
    //         addClassActive: true,
    //         responsiveRefreshRate: 200,
    //         theme: 'slideshow-3',
    //         beforeInit: false,
    //         afterInit: false,
    //         beforeMove: false,
    //         afterMove: false,
    //         startDragging: false,
    //         afterAction: false,
    //         afterLazyLoad: false

    //     });

    // });     

    
    $(function() {
        var owl = $('.sliderCarousel');
        if (owl.length==0) {return}
        owl.owlCarousel({
            navigation: true,
            loop:false,
            pagination: true,
            autoHeight: false,
            autoPlay: 6000,
            slideSpeed: 900,
            items: 1,
            itemsScaleUp: true,
            singleItem: true,
            addClassActive: true,
            responsiveRefreshRate: 200,
            theme: 'slideshow-3',
            beforeInit: false,
            afterInit: false,
            beforeMove: false,
            afterMove: false,
            startDragging: false,
            afterAction: false,
            afterLazyLoad: false

        });

    });     

},300)