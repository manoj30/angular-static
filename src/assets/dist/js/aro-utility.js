var AroUtility = function() {

    var $html, $body, $headerPos, $bodyWrap, $menu, $headerAffixClone, $ctrl, scrollPos, $scrollBtn;

    var init = function( $name, $element )
    {
        if ($name == 'checkHeightChange') {
            checkHeightChange( $element );
            $(window).resize(function() {
                if ($(window).width() > 768) checkHeightChange( $element );
            });
        }
    };

    var checkHeightChange = function( $header )
    {
        $headerAffixClone = $('.header-affix-clone');
        $headerAffixClone.css({height: $header.height() + 'px'});
    };

    var checkFlipCompatibility = function( $element )
    {
        // Used to disable the flip effect on ALL non-compatible browsers,
        // if the browser is in-compatible it defaults to showing the back
        // face of the element.
        $html = $('html');
        $body = $('body');
        if (   $body.hasClass('is-browser-ie')
            || $html.hasClass('no-csstransforms3d') ){
            $element.addClass('hover');
        }
    };

    var rippleAnimation = function(  )
    {
        $menu = $(".mobile-menu-nav li a:not(.dropdown-toggle)");
        $menu.click(function(evt)
        {
            var elem, display, ripple, max, x, y;

            elem = $(this);
            elem.prepend("<span class='ripple'></span>");

            display = elem.css("display");
            if ( display == 'inline' || display == 'inline-block')
            {
                elem.css({display:'inline-block'});
            }
            else
            {
                elem.css({display:'block'});
            }

            ripple = elem.find('.ripple');
            ripple.removeClass('animate');

            if ( !ripple.width() && !ripple.height() )
            {
                max = Math.max(elem.outerWidth(),elem.outerHeight());
                ripple.css({width:max+'px',height:max+'px'});
            }

            x = evt.pageX - elem.offset().left - ripple.width()/2;
            y = evt.pageY - elem.offset().top - ripple.height()/2;

            ripple.css({top:y+'px',left:x+'px'});

            setTimeout(function(){
                ripple.addClass('animate');
            }, 1);
        });
    };

    var headerBanner = function( triggerOnScroll )
    {
        $body      = $('body');
        $body.addClass('header-banner-control');
        $headerPos = $('.header-wrap').offset().top;
        $ctrl      = $('html, .header-banner-control');
        $scrollBtn = $('.header-banner-scroll-btn');

        if (typeof triggerOnScroll !== 'undefined') {
            $ctrl.on('scroll', function(){
                scrollPos = $body.scrollTop();
                if (scrollPos >= 10) {
                    scrollPage($ctrl, $headerPos);
                    $ctrl.off('scroll');
                }
            });
        }

        $scrollBtn.on('click', function(e){
            e.preventDefault();
            $ctrl.off('scroll');
            scrollPage($ctrl, $headerPos);
        });
    };

    var scrollPage = function($ctrl, $headerPos)
    {
        $bodyWrap = $('.body-wrap');
        $ctrl.animate({
            scrollTop: $headerPos + 1 // accounts for border
        }, 900);
    };

    return {
        init: function( $name, $element ) {
            init( $name, $element );
        },
        checkHeightChange: function( $header ) {
            checkHeightChange( $header );
        },
        checkFlipCompatibility: function( $element ) {
            checkFlipCompatibility( $element );
        },
        rippleAnimation: function() {
            rippleAnimation();
        },
        headerBanner: function(triggerOnScroll) {
            headerBanner(triggerOnScroll);
        }
    };
}();
