import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DefaultLayoutComponent } from "./default-layout.component";

const routes: Routes = [
  {
    path: "",
    component: DefaultLayoutComponent,
    children: [
      {
        path: "dashboard",
        loadChildren: () => import('../../views/dashboard/dashboard.module').then(mod => mod.DashboardModule)
      },
      {
        path: "users",
        loadChildren: () => import('../../views/users/users.module').then(mod => mod.UsersModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DefaultRoutingModule {}
