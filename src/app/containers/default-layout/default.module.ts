import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultRoutingModule } from './default-routing.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { DefaultLayoutComponent } from './default-layout.component';
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  AppAsideModule,
  // AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule
} from "@coreui/angular";


@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    AppAsideModule,
    AppHeaderModule,
    AppFooterModule,
    PerfectScrollbarModule,
    // AppBreadcrumbModule.forRoot(),
    DefaultRoutingModule,
    BsDropdownModule.forRoot(),
  ],
  declarations: [DefaultLayoutComponent, DefaultLayoutComponent],
  providers: [{ provide: BsDropdownModule, useValue: { isAnimated: true, autoClose: true } }]
})
export class DefaultModule {
}
