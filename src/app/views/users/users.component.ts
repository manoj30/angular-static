import { Component, OnInit } from '@angular/core';
import { Filter } from './users.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  order: string = 'firstName';
  reverse: boolean = false;
  dataFilter: Filter = new Filter;
  setDataFilter: any;
  setFilter(){
    this.setDataFilter = Object.assign({}, this.dataFilter);
  }
  resetFilter(){
    this.dataFilter = new Filter;
    this.setDataFilter = { };
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }

  constructor() { }

  users: any = [
    {
      firstName: "David",
      surname: "Smith",
      role: "hign Level Admin",
      organization: "Smart Lotto"
    },
    {
      firstName: "Duncan",
      surname: "Ryan",
      role: "SL Personal Admin",
      organization: "Smart Lotto"
    },
    {
      firstName: "Mariah",
      surname: "Maclachlon",
      role: "SL Personal Admin",
      organization: "Smart Lotto"
    }
  ]

  ngOnInit() {
  }



}
