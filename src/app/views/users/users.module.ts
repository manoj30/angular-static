import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { OrderModule } from 'ngx-order-pipe';
import {
  AppAsideModule,
  AppSidebarModule
} from "@coreui/angular";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GrdFilterPipe } from './../../services/grd-filter.pipe';

@NgModule({
  declarations: [ UsersComponent, GrdFilterPipe ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    OrderModule,
    AppSidebarModule,
    AppAsideModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UsersModule { }
