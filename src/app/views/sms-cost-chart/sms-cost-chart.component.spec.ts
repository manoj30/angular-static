import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsCostChartComponent } from './sms-cost-chart.component';

describe('SmsCostChartComponent', () => {
  let component: SmsCostChartComponent;
  let fixture: ComponentFixture<SmsCostChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsCostChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsCostChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
