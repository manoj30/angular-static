import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sms-cost-chart',
  templateUrl: './sms-cost-chart.component.html',
  styleUrls: ['./sms-cost-chart.component.scss']
})
export class SmsCostChartComponent implements OnInit {

  constructor() { }

  public barChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public barChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];

  public barChartData = [
    { data: [150000, 200000, 300000, 400000, 300000, 270000, 450000, 350000, 450000, 350000, 500000, 470000] },
  ];

  ngOnInit() {
  }

}
