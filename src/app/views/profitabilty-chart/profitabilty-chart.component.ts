import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profitabilty-chart',
  templateUrl: './profitabilty-chart.component.html',
  styleUrls: ['./profitabilty-chart.component.scss']
})
export class ProfitabiltyChartComponent implements OnInit {

  constructor() { }

  public radarChartOptions = {
    responsive: true,
  };
  public radarChartLabels = ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'];

  public radarChartData = [
    { data: [65, 59, 90, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 96, 27, 100], label: 'Series B' }
  ];
  public radarChartType = 'line';

  ngOnInit() {
  }

}
