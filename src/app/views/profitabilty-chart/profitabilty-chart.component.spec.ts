import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitabiltyChartComponent } from './profitabilty-chart.component';

describe('ProfitabiltyChartComponent', () => {
  let component: ProfitabiltyChartComponent;
  let fixture: ComponentFixture<ProfitabiltyChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfitabiltyChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitabiltyChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
