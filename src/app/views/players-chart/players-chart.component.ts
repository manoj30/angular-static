import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-players-chart',
  templateUrl: './players-chart.component.html',
  styleUrls: ['./players-chart.component.scss']
})
export class PlayersChartComponent implements OnInit {

  constructor() { }

  public doughnutChartLabels = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData = [
    [350, 450, 100],
    [50, 150, 120],
    [250, 130, 70],
  ];
  public doughnutChartType = 'doughnut';

  ngOnInit() {
  }

}
