import { Component, OnInit, ViewChild, Input } from '@angular/core';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  order: string = 'number';
  reverse: boolean = false;
  
  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }

  constructor(
   
  ) { }
  
  ngOnInit() {
    
  }

  techSupport = [
    {
      number:"320",
      name:"Club Admin",
      description:"Draw Accidently turned off",
      dateOpened:"13/08/2019",
      status:"Resolved"
    },
    {
      number:"319",
      name:"Marco Botton",
      description:"Last Password",
      dateOpened:"12/08/2019",
      status:"New"
    },
    {
      number:"318",
      name:"Maria McTigue",
      description:"Email account disabled",
      dateOpened:"11/8/2019",
      status:"In Progress"
    }
  ]
}
