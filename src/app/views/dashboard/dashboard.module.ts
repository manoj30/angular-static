import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ChartsModule } from 'ng2-charts';
import { SmsCostChartComponent } from '../sms-cost-chart/sms-cost-chart.component';
import { PlayersChartComponent } from '../players-chart/players-chart.component';
import { ProfitabiltyChartComponent } from '../profitabilty-chart/profitabilty-chart.component';
import { SalesChartComponent } from '../sales-chart/sales-chart.component';
import { OrderModule } from 'ngx-order-pipe';
import {
  AppAsideModule,
  AppSidebarModule
} from "@coreui/angular";

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    DashboardRoutingModule,
    OrderModule,
    AppSidebarModule,
  AppAsideModule
  ],
  declarations: [DashboardComponent, SmsCostChartComponent, PlayersChartComponent, ProfitabiltyChartComponent, SalesChartComponent],
  providers: []
})
export class DashboardModule {
}
