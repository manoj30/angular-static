interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'Campaign',
    url: '/campaign',
    icon: 'icon-list',
    children: [
      {
        name: 'Create Campaign',
        url: '/campaign/create'
        // icon: 'fa fa-users'
      },
      {
        name: 'Manage Campaign',
        url: '/campaign/manage'
        // icon: 'fa fa-users'
      },
      {
        name: 'Permission Request',
        url: '/campaign/request'
        // icon: 'fa fa-users'
      }
    ]
  },
  {
    name: 'Reports',
    url: '/reports',
    icon: 'icon-list',
    children: [
      {
        name: 'Campaign Reports',
        url: '/campaign-reports'
        // icon: 'fa fa-users'
      },
      {
        name: 'Publisher Wise Reports',
        url: '/publisher-reports'
        // icon: 'fa fa-users'
      },
      {
        name: 'Advirtiser Wise Reports',
        url: '/advirtiser-reports'
        // icon: 'fa fa-users'
      },
      {
        name: 'Click Log',
        url: '/click-log'
        // icon: 'fa fa-users'
      },
      {
        name: 'Postback Log',
        url: '/postback-log'
        // icon: 'fa fa-users'
      },
      {
        name: 'Conversion Log',
        url: '/conversion-log'
        // icon: 'fa fa-users'
      }
    ]
  },
  {
    name: 'Publisher',
    url: '/publisher',
    icon: 'icon-user',
    children: [
      {
        name: 'Add Publisher',
        url: '/publisher/add'
        // icon: 'fa fa-users'
      },
      {
        name: 'Manage/Edit Publisher',
        url: '/publisher/manage'
        // icon: 'fa fa-users'
      },
      {
        name: 'Add Postback/Pixel',
        url: '/publisher/postback'
        // icon: 'fa fa-users'
      }
    ]
  },
  {
    name: 'Advertiser',
    url: '/advertiser',
    icon: 'icon-user',
    children: [
      {
        name:'Add advertiser',
        url:'/advertiser/add'
      },
      {
        name:'Manage/ Edit Advirtiser',
        url:'/advertiser/manage'
      }
    ]
  }
 
];
